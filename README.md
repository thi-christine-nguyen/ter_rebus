# TER • Rébus

## Description

Le but de ce TER est de générer des rébus.

## Auteurs

Ce TER est réalisé par les membres du **Groupe H** :

| Nom     | Prénom        | Groupe de TD |
|---------|---------------|--------------|
| Azeb    | Melissa       | Groupe B     |
| Bonin   | Zoé           | Groupe B     |
| Laviron | Pablo         | Groupe B     |
| Nguyen  | Thi-Christine | Groupe B     |

## Installation

Instructions pour installer le projet.

> Vous devez avoir installé [Python](https://www.python.org/downloads/) (3.7 minimum), ainsi que [Perl](https://www.perl.org/get.html) (pré-installé sur Linux et macOS).

Commencez par cloner ce dépôt GIT et rendez vous dans le dossier créé :

```bash
$ git clone https://gitlab.com/thi-christine-nguyen/ter_rebus.git
$ cd ter_rebus
```

Ensuite, suivez les instructions correspondantes à votre système d'exploitation.

Linux / macOS :

```bash
$ python3 -m venv venv
$ . venv/bin/activate
$ pip install -r requirements.txt
```

Windows :

```bash
$ py -3 -m venv venv
$ venv\Scripts\activate
$ pip install -r requirements.txt
```

## Lancement

Instructions pour lancer l'application.

```bash
$ flask run                 # Option --debug facultative
```

> Par défaut, l'application est accessible sur le **port 5000**. Vous pouvez spécifier un port avec l'option `--port=<votre_port>`.

Si la commande ci-dessus ne fonctionne pas, vous pouvez utiliser les suivantes :

```bash
$ python3 app.py            # Linux / macOS
$ py -3 app.py              # Windows
```


## Sauvegarde

Instructions pour enregistrer la version hébergée sur PythonAnywhere.

```bash
$ tar -czvf backup.tar.gz /home/<votre_nom_d_utilisateur>/
```

```bash
$ wget https://eu.pythonanywhere.com/user/<votre_nom_d_utilisateur>/files/home/<votre_nom_d_utilisateur>/backup.tar.gz
```
