\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{amsmath}
\usetikzlibrary{positioning,shapes.multipart,shapes,arrows.meta}

\title{Compte Rendu Semaine 1}
\author{Groupe H}
\date{\today}

\begin{document}

\tikzset{
  entity/.style={
    draw,
    rectangle split,
    rectangle split parts=2,
    rectangle split part fill={blue!20,white},
    minimum width=2.5cm,
    text width=2cm,
    align=left
  },
  relationship/.style={
    ellipse,
    draw,
    text centered,
    fill=blue!10!white
  }
}

\maketitle

\abstract{
  Ce compte rendu a pour objectif de résumer l'avancée du travail durant la première semaine sur le projet de programmation <<Générateur de rébus>>.
}


\section{Introduction}
  Durant cette première semaine, nous avons testé beaucoup de choses pour évaluer ce qui est possible, et ce qui l'est moins. Nous avons aussi mis en place des outils et nous nous sommes mis d'accord sur les technologies utilisées dans notre application. Nous avons décidé de ne pas nous intéresser au côté interface graphique pour le moment, en attendant d'avoir un algorithme fonctionnel ainsi qu'une base de données assez remplie. Nous ne nous sommes pas encore intéressé au problème du remplissage de la base de données.

\section{Travail en groupe}
  Pour faciliter l'avancement du projet, nous avons décidé de nous scinder en deux groupes : un groupe chargé de réaliser l'algorithme principal de génération des rébus, et un groupe chargé de modéliser et remplir la base de données. Cela permet de travailler en \textit{pair programming} parallèlement, et donc efficacement.
  
  Pour travailler en groupe tout au long du développement de notre application, nous utilisons la plateforme GitLab, qui offre un dépôt Git ainsi qu'un tableau <<Kanban>> pour assigner des tâches aux membres du groupe. GitLab propose une interface de \textit{Pull Request}, permettant à tout le monde de suivre l'évolution des différentes parties du projet, avec un aperçu des modifications avant la fusion avec la branche principale.

\section{Technologies et langages utilisés}
  Nous avons choisi de séparer notre application en deux parties : un serveur pour le \textit{backend}, notre API (Application Programming Interface), ainsi qu'un serveur pour le \textit{frontend}, partie interface graphique. L'interface graphique communique avec l'API, qui communique avec la base de données.

\vspace{0.5cm}

\begin{figure}[h]
  \center
  \begin{tikzpicture}
    \node[draw,rectangle,align=left] (Backend) {\textbf{Backend (API)} \\ Python, Flask};
    \node[draw,rectangle,align=left,left=3cm of Backend] (BDD) {\textbf{Base de données} \\ SQLite};
    \node[draw,rectangle,align=left,right=3cm of Backend] (Frontend) {\textbf{Frontend} \\ Javascript};
    \draw[arrows = {-Stealth[length=10pt]}] (BDD) to (Backend);
    \draw[arrows = {-Stealth[length=10pt]}] (Backend) to (BDD);
    \draw[arrows = {-Stealth[length=10pt]}] (Frontend) to (Backend);
    \draw[arrows = {-Stealth[length=10pt]}] (Backend) to (Frontend);
  \end{tikzpicture}
  \caption{Fonctionnement de l'application}
\end{figure}

\subsection{Création de l'API}  
  Le cœur de notre application se trouve dans l'API : c'est ici que les rébus seront générés. Nous avons choisi de la développer dans le langage Python. En effet, Python est assez flexible pour convenir à tous nos besoins, que ce soit pour le développement de l'algorithme principal mais aussi pour la gestion de la base de données et du serveur. Nous utilisons le \textit{framework} Flask pour gérer le serveur, couplé avec la bibliothèque SQLAlchemy, un ORM (Object-Relational Mapping) permettant de faire le lien entre des classes Python et les tables de la base de données. Comme la vitesse d'exécution de notre algorithme est importante, nous utilisons une base de données locale SQLite, sous forme de fichier binaire.
  
\subsection{Recherche d'un phonétiseur}
  Une partie importante de notre application est la conversion d'un mot en liste de phonèmes. Nous avons donc cherché si un tel programme existait, et nous nous sommes assez rapidement rendu compte qu'il y en avait peu pour le langage français. En effet, plusieurs modules Python proposent une telle conversion pour un mot en anglais (par exemple Soundex), mais ne supportent pas d'autres langues. Il se trouve que M. Pompidor, un de nos encadrant, avait développé un tel programme en 2012 dans le langage Perl, et nous a autorisé à l'utiliser dans le cadre de notre projet. Après quelques modification, nous avons écrit un module Python permettant d'utiliser ce programme Perl dans notre API.

\section{Base de données et recherche d'image}

\subsection{Base de données}
  Notre base de données a beaucoup changé durant cette semaine : initialement, nous avions prévu de séparer les phonèmes qui composent un mot dans une table à part, mais lors de la réflexion faite pour l'écriture de l'algorithme, nous nous sommes rendu compte qu'une seule table suffisait. Nous avons donc mis les phonèmes sous la forme d'une chaîne de caractère, comme attribut de la table contenant les mots.
  
  Voici les deux modèles Entité-Association de notre base de données.

  \vspace{0.5cm}
  
  \begin{figure}[h]
    \center
    \begin{tikzpicture}
      \node[entity] (MotImage) {MotImage \nodepart{second} \underline{mot} \\ image \\ nbPhonemes};
      \node[entity,right=5cm of MotImage] (Phoneme) {Phoneme \nodepart{second} \underline{phoneme}};
      \draw (MotImage) -- (Phoneme) node[midway,relationship] (Compose) {Compose};
      
      \node[below=0.5cm of Compose] (position) {position}; 
      \draw (Compose) -- (position);
    \end{tikzpicture}
    \caption{Premier modèle E/A}
  \end{figure}
  
  \vspace{0.5cm}

  \begin{figure}[h]
    \center
    \begin{tikzpicture}
      \node[entity] (MotImage) {MotImage \nodepart{second} \underline{mot} \\ image \\ phonemes};
    \end{tikzpicture}
    \caption{Second modèle E/A}
  \end{figure}

\subsection{Recherche d'image}
  La rechercher d'une image correspondante à un mot est une partie importante lorsqu'il s'agit de construire une application où le rendu visuel est au cœur même des spécifications. L'essence même d'un rébus est dans ses images : dans la base de données, chaque mot pouvant composer un rébus doit être associée à une image. Mais étant donné le nombre important de mots pouvant composer un rébus, nous n'allons pas rechercher à la main une image pour chaque mot. C'est comme ça que nous est venu une première idée : automatiser la recherche d'image.
  
  \newpage
  
  Le \textit{web scraping} est une méthode qui consiste à obtenir des données à partir d'un site Internet. Comme première idée, nous avons utilisé cette méthode pour récupérer des images à partir du site Google Images. Ce procédé se divisait en trois parties :
  
  \begin{enumerate}
    \item
    Envoie d'une requête HTTP vers l'URL de Google Image avec un mot à rechercher
    \item
    Récupération de la page HTML renvoyée dans un objet
    \item
    Recherche de la première balise image dans la page et récupération de sa source (URL)
  \end{enumerate}

  Cette méthode fonctionnait relativement bien, mais nous avons remarqué quelques problèmes :
  \begin{itemize}
    \item
    La plupart des images n'étaient pas libres de droits
    \item
    Les images sponsorisées apparaissaient en premier
    \item
    Nous étions limité à des images d'aperçu, de petites taille
  \end{itemize}

  Nous avons également essayé d'utiliser la banque d'image de Wikimedia Commons au lieu de Google Images, qui elle propose des images libres, mais les résultats n'étaient pas assez pertinents.
  
  Ce qui nous a amené à une seconde solution : plutôt que de rechercher des images, générer des images. En utilisant l'API d'OpenAI, nous pouvons générer une image à partir d'un texte (de manière similaire à DALL$\cdot$E) et récupérer ses données. Ces images ont l'avantage d'être libres de droits et de plus grandes tailles, mais nous perdons en fiabilité car le texte doit être en anglais (on pourrait éventuellement traduire le texte avant l'envoi de la requête). De plus, nous sommes limités à une version gratuite de l'API, qui comprend environ 1.000 requêtes.
  
  En pratique, la génération d'image est plus lente que la recherche d'image, mais ce n'est pas un problème pour notre application puisque la base de données sera remplie au préalable.

\section{Algorithme principal}
  L'algorithme principal permet de générer un rébus (sous la forme d'une liste de mots) à partir d'une phrase. Des images seront ensuite associées à chacun de ces mots. Sa signature est la suivante :
  
  \textbf{Entrée :} Phrase $P$ de mots $p_{1}, \dots, p_{i}$ séparés par des espaces.
  
  \textbf{Sortie :} Liste $L$ de mots $l_{1}, \dots, l_{j}$ tel que $\forall l_{k} \in L, phonemes(l_{k}) \leq n$ et qui, lus de gauche à droite, ont une sonorité similaire à $P$.
    
  Nous avons cherché un algorithme permettant de trouver une solution optimale à notre problème de création de rébus à partir d'une phrase, et nous sommes parvenus à trouver deux idées de stratégies pour le résoudre :

\begin{enumerate}
  \item
  En partant du premier phonème composant la phrase, on récupère tous les mots commençant par ce phonème. Puis parmi ces mots, on regarde si certains contiennent le deuxième phonème de la phrase, et ainsi de suite. Si on ne trouve pas de phonème, on retourne en arrière.
  
  \vspace{0.5cm}
  
  \begin{figure}[h]
    \center
    \begin{tikzpicture}
      \node[draw,ellipse,align=center] (JurassicParc) {Jurassic Parc \\ \texttt{\textcolor{green!50!black}{Zy} \textcolor{orange!70!black}{Ra} \textcolor{cyan!50!black}{si} k pa Rk}};
      
      \node[draw,circle,align=center,below=0.5cm of JurassicParc] (Judo) {Judo \\ \texttt{Zy do}};
      \node[draw,circle,fill=green!30!lightgray,align=center,left=0.5cm of Judo] (Jus) {Jus \\ \texttt{Zy}};
      \node[draw,circle,align=center,right=0.5cm of Judo] (Jury) {Jury \\ \texttt{Zy Ri}};
      
      \node[draw,circle,align=center,below=0.5cm of Jus] (Rame) {Rame \\ \texttt{Ra m}};
      \node[draw,circle,fill=orange!40!lightgray,align=center,left=0.5cm of Rame] (Rat) {Rat \\ \texttt{Ra}};
      \node[draw,circle,align=center,right=0.5cm of Rame] (Radis) {Radis \\ \texttt{Ra di}};
      
      \node[draw,circle,align=center,below=0.5cm of Rat] (Citerne) {Citerne \\ \texttt{si tE Rn}};
      \node[draw,circle,fill=cyan!30!lightgray,align=center,left=0.5cm of Citerne] (Si) {Si \\ \texttt{si}};
      \node[draw,circle,align=center,right=0.5cm of Citerne] (Citron) {Citron \\ \texttt{si tR§}};
      
      \node[align=center,below=0.5cm of Si] (etc) {\\ \dots};
      
      \draw[arrows = {-Stealth[length=5pt]}] (JurassicParc) to (Judo);
      \draw[arrows = {-Stealth[length=5pt]}] (JurassicParc) to (Jus);
      \draw[arrows = {-Stealth[length=5pt]}] (JurassicParc) to (Jury);
      
      \draw[arrows = {-Stealth[length=5pt]}] (Jus) to (Rame);
      \draw[arrows = {-Stealth[length=5pt]}] (Jus) to (Rat);
      \draw[arrows = {-Stealth[length=5pt]}] (Jus) to (Radis);
      
      \draw[arrows = {-Stealth[length=5pt]}] (Rat) to (Citerne);
      \draw[arrows = {-Stealth[length=5pt]}] (Rat) to (Si);
      \draw[arrows = {-Stealth[length=5pt]}] (Rat) to (Citron);
      
      \draw[arrows = {-Stealth[length=5pt]}] (Si) to (etc);
    \end{tikzpicture}
    \caption{Exemple de la stratégie 1 sur <<Jurassic Parc>>}
  \end{figure}
  
  \newpage
  
  \item
  Soit $n$ le nombre maximum de phonèmes d'un mot dans la base de données. On cherche un mot de $n$ phonèmes tel que ses phonèmes soient égaux aux $n$ premiers phonèmes de la phrase. Si ce mot existe, on le prend et on relance l'algorithme sur la suite de la phrase. Sinon, on réessaie avec les $n-1$ premiers phonèmes de la phrase, et ainsi de suite jusqu'à la fin de la phrase.
  
  \vspace{0.5cm}
  
  \begin{figure}[h]
    \center
    \begin{tikzpicture}
      \node[align=left] (JP) {Jurassic Parc : \\ \texttt{Zy Ra si k pa Rk}};
      
      \node[align=left,right=1cm of JP] (n) {$n=3$};
    
      \node[align=center,below=1cm of JP] (JP1) {$\underbrace{\textcolor{green!50!black}{\texttt{Zy Ra si}}}_\text{\O} \texttt{ k pa Rk}$};
      
      \node[align=center,below=0.5cm of JP1] (JP2) {$\underbrace{\textcolor{green!50!black}{\texttt{Zy Ra}}}_\text{\O} \texttt{ si k pa Rk}$};
      
      \node[align=center,below=0.5cm of JP2] (JP3) {$\underbrace{\textcolor{green!50!black}{\texttt{Zy}}}_\text{} \texttt{Ra si k pa Rk}$};
      
      \node[align=center,below=0cm of JP3] (JP4) {Mot <<Jus>>};
      
      
      \node[align=center,right=1cm of JP1] (JP5) {$\texttt{\textcolor{green!50!black}{Zy} }\underbrace{\texttt{\textcolor{orange!70!black}{Ra si k}}}_\text{\O} \texttt{ pa Rk}$};
      
      \node[align=center,right=1cm of JP2] (JP6) {$\texttt{\textcolor{green!50!black}{Zy} }\underbrace{\texttt{\textcolor{orange!70!black}{Ra si}}}_\text{\O} \texttt{ k pa Rk}$};
      
      \node[align=center,right=1cm of JP3] (JP7) {$\texttt{\textcolor{green!50!black}{Zy} }\underbrace{\texttt{\textcolor{orange!70!black}{Ra}}}_\text{} \texttt{ si k pa Rk}$};
      
      \node[align=center,below=0cm of JP7] (JP8) {Mot <<Rat>>};
      
      
      \node[align=center,right=1cm of JP5] (JP9) {$\texttt{\textcolor{green!50!black}{Zy} \textcolor{orange!70!black}{Ra} }\underbrace{\texttt{\textcolor{cyan!50!black}{si k pa}}}_\text{\O} \texttt{ Rk}$};
      
      \node[align=center,right=1cm of JP6] (JP10) {$\texttt{\textcolor{green!50!black}{Zy} \textcolor{orange!70!black}{Ra} }\underbrace{\texttt{\textcolor{cyan!50!black}{si k}}}_\text{\O} \texttt{ pa Rk}$};
      
      \node[align=center,right=0.7cm of JP7] (JP11) {$\texttt{\textcolor{green!50!black}{Zy} \textcolor{orange!70!black}{Ra} }\underbrace{\texttt{\textcolor{cyan!50!black}{si}}}_\text{} \texttt{ k pa Rk}$};
      
      \node[align=center,below=0cm of JP11] (JP12) {Mot <<Si>>};
      
      
      \node[align=center,right=1cm of JP10] (JP13) {\dots};

    \end{tikzpicture}
    \caption{Exemple de la stratégie 2 sur <<Jurassic Parc>>}
  \end{figure}
\end{enumerate}

  Le problème de ces stratégies est qu'elles cherchent une solution optimale paramétrée par le nombre maximum de phonèmes, mais pas forcément une solution parfaite, dans le sens où il se peut que le rébus obtenu contienne des lettres seules ou des mots qui n'existent pas.


\end{document}