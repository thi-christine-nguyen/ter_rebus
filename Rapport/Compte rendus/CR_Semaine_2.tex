\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{ulem}
\usetikzlibrary{positioning,shapes.multipart,shapes,arrows.meta}

\title{Compte Rendu Semaine 2}
\author{Groupe H}
\date{\today}

\begin{document}

\tikzset{
  entity/.style={
    draw,
    rectangle split,
    rectangle split parts=2,
    rectangle split part fill={blue!20,white},
    minimum width=2.5cm,
    text width=2cm,
    align=left
  },
  relationship/.style={
    ellipse,
    draw,
    text centered,
    fill=blue!10!white
  }
}

\maketitle

\abstract{
  Ce compte rendu a pour objectif de résumer l'avancée du travail durant la deuxième semaine sur le projet de programmation <<Générateur de rébus>>.
}


\section{Introduction}
  Durant cette deuxième semaine, nous nous sommes intéressé au problème du remplissage de la base de données, et nous avons avancé sur la partie algorithmique, avec de nouvelles stratégies se voulant plus précises que celles de la semaine dernière.

\section{Remplissage de la base de données}
  Cette semaine, nous nous sommes intéressé au problème du remplissage de la base de données : jusqu'ici, la base ne comportait qu'une dizaine de mots, qui étaient utilisés pour faire des tests sur l'algorithme de génération de rébus.

  Tout d'abord, nous avons cherché s'il n'existait pas déjà une liste des mots utilisés dans les rébus. Nous n'avons pas trouvé une telle liste, donc nous étions obligés de trouver une approche pour la rédiger.

  La \textbf{première approche} qui nous est venue naturellement était d'ajouter les mots <<en dur>> dans la base de données. Bien que cette approche soit très empirique, elle a l'avantage de ne pas avoir de mauvais mots dans la base de données puisque seuls ceux que nous écrivons sont ajoutés. Évidemment, ce n'est ni une solution viable à long terme, ni une solution compatible avec les méthodes de travail Agile (il faudrait synchroniser le dépôt GIT à chaque fois qu'un membre du groupe ajoute un mot\dots). Nous avons donc abandonné cette méthode, mais nous l'avons gardé en tête.

  La \textbf{deuxième approche} est une approche de \textit{web scrapping} : cela consiste à extraire une partie d'une page web et manipuler ces données. Dans notre cas, nous avons extrait d'une page web les 600 mots français les plus utilisés, en vue de les ajouter automatiquement dans notre base de données. En ne prenant que  les adjectifs et les noms communs ayant au plus 3 phonèmes, nous sommes arrivés à un total de 346 mots. Une grosse différence avec la première approche est la quantité de données : ici, nous avons pris les 600 mots français les plus utilisés, mais nous aurions pu en prendre bien plus (2.000 par exemple). En revanche, il ne s'agit pas des mots les plus utilisés dans des rébus, mais des mots utilisés dans la littérature (livres, journaux, médias\dots) ce qui relève une nouvelle question par rapport à cette deuxième approche : bien que l'on ai obtenu une grande quantité de mots, est-ce que ces mots sont réellement utilisables dans des rébus ? Nous n'avons pas encore la réponse à cette question, car nous attendons d'avoir un algorithme de génération de rébus fonctionnel pour tester cette version de la liste de mots.
  
  La \textbf{troisième approche} consiste à utiliser une base de données collaborative. Cette idée nous est venue après l'idée du web scrapping, en nous rappelant de la première approche, empirique certes, mais plus précise. L'idée derrière cette méthode est d'utiliser le facteur humain pour rédiger notre liste de mots sans être limité à notre groupe de 4, mais à tous ceux qui souhaitent améliorer notre liste de mots. Nous avons alors créé et hébergé une plateforme en ligne pour la collecte des mots.

\section{Plateforme de collecte de mots pour les rébus}

\subsection{Technologies utilisées}
  Nous avons choisi de développer cette plateforme avec les mêmes outils que notre application principale (Python, Flask et SQLite). Cela permettra, au moment venu, de fusionner les deux applications si nécessaire. Nous avons trouvé un hébergeur de serveur Flask gratuit (Pythonanywhere) et nous avons rapidement déployé notre plateforme dessus pour pouvoir commencer au plus tôt possible à ajouter des mots, et à donner le lien à nos proches pour les inviter à en faire autant.

\subsection{Fonctionnement}
  L'idée est simple : un utilisateur entre un mot, et le mot est ajouté à la base de données. Ensuite, on peut télécharger un fichier contenant tous les mots sous format JSON, puis dans notre application principale, une fonction lit le JSON et ajoute tous les mots.
  
  Afin d'éviter les abus, nous avons mis en place un système de vérification : lorsqu'un utilisateur entre un mot, il est soumis à une validation par un administrateur avant d'être ajouté dans la liste des mots. Les administrateurs (les membres de notre groupe) peuvent alors accepter le mot ou le refuser. Le schéma ci-dessous résume le fonctionnement de notre plateforme :
  
\vspace{0.5cm}

\begin{figure}[h]
  \center
  \begin{tikzpicture}
    \node[draw,rectangle,align=left] (verif) {\textbf{Vérification} \\ Mot dans la table \\ \texttt{MotAVerifier}};
    \node[align=left,left=3cm of verif] (mot) {\texttt{mot à ajouter}};
    \node[align=left,right=3cm of verif] (mid) {};
    \node[draw,rectangle,align=left,above=1cm of mid] (accepte) {\textbf{Mot accepté} \\ Ajout du mot \\ dans la table \\ \texttt{MotImage}};
    \node[draw,rectangle,align=left,below=1cm of mid] (refuse) {\textbf{Mot refusé} \\ Suppression du \\ mot de la table \\ \texttt{MotAVerifier}};
    \draw[arrows = {-Stealth[length=10pt]}] (mot) to (verif);
    \draw[arrows = {-Stealth[length=10pt]}] (verif) to (accepte);
    \draw[arrows = {-Stealth[length=10pt]}] (verif) to (refuse);
  \end{tikzpicture}
  \caption{Fonctionnement de la plateforme}
\end{figure}

\subsection{Base de données}
  Voici le modèle Entité-Association de la base de données utilisée dans cette plateforme.
  
\vspace{0.5cm}

\begin{figure}[h]
  \center
  \begin{tikzpicture}
    \node[entity] (MotAVerifier) {MotAVerifier \nodepart{second} \underline{mot} \\ image};
    \node[entity, right=1cm of MotAVerifier] (MotImage) {MotImage \nodepart{second} \underline{mot} \\ image};
  \end{tikzpicture}
  \caption{Modèle E/A}
\end{figure}
  
  On note l'absence de l'attribut <<phonèmes>> : comme il s'agit d'une plateforme parallèle, nous avons décider de ne pas ajouter l'algorithme de phonétisation ici, et l'application principale se chargera de calculer les phonèmes.
  
  On note aussi la présence de l'attribut <<image>> : initialement, nous avions prévu que l'utilisateur puisse aussi déposer une image, mais nous nous sommes rendu compte que ce n'était pas une bonne idée puisqu'il y a de grande chances que l'utilisateur dépose une image non libre de droits (par exemple la première sur Google Image), soit par ignorance des droits d'auteurs, soit par manque de temps.

\newpage

\section{Stratégie d'algorithme précis}
  Concernant l'algorithme, une nouvelle stratégie nous est venue. Ce nouvel algorithme donne une meilleure solution en utilisant du \textit{backtracking}. Le rébus est alors plus précis et a moins de chance de renvoyer un rébus incomplet. L'algorithme est le suivant : les possibilités sont rangés selon leur position dans le rébus. Les premiers mots choisis ont une position correspondant au premier élément du rébus, cela va permettre de ne pas tout parcourir. À chaque itération, on rajoute un mot et on voit s'il est compatible avec les mots déjà présents dans solution.
  
  \vspace{0.5cm}
  
  \begin{figure}[h]
    \center
    \begin{tikzpicture}
      \node[draw,ellipse,align=center] (toutes) {Toutes les possibilités : \\ $P=[mot_1, \dots, mot_n]$};
      
      \node[align=center,below=1cm of toutes] (mid1) {};
      \node[draw,ellipse,align=center,left=0.5cm of mid1] (mot1) {Solution : \\ $[mot_1]$};
      \node[draw,ellipse,align=center,right=0.5cm of mid1] (mot2) {Solution : \\ $[mot_2]$};
      
      \node[align=center,below=1cm of mot1] (mid2) {};
      \node[draw,ellipse,align=center,left=0.5cm of mid2] (mot1mot2) {Solution : \\ $[mot_1, mot_2]$};
      \node[draw,ellipse,align=center,right=0.5cm of mid2] (mot1mot3) {Solution : \\ $[mot_1, mot_3]$};
      
      \node[align=center,below=1cm of mot1mot2] (incomp) {N'est pas compatible};
      
      \node[align=center,left=0.5cm of mot1] (Pmot1) {$P=[$\sout{$mot_1$}$, mot_2, mot_3, \dots]$};
      
      \node[align=center,left=0.5cm of mot1mot2] (Pmot1mot2) {$P=[$\sout{$mot_1, mot_2$}$, mot_3, \dots]$};
      
      \node[align=center,right=0.5cm of mot1mot3] (Pmot1mot3) {$P=[$\sout{$mot_1, mot_2$}$, mot_3, \dots]$};
      
      \node[align=center,below=0.75cm of mot1mot3] (etc) {\\ \dots};
      
      \draw[arrows = {-Stealth[length=5pt]}] (toutes) to (mot1);
      \draw[arrows = {-Stealth[length=5pt]}] (toutes) to (mot2);
      
      \draw[arrows = {-Stealth[length=5pt]}] (mot1) to (mot1mot2);
      \draw[arrows = {-Stealth[length=5pt]}] (mot1) to (mot1mot3);
      
      \draw[arrows = {-Stealth[length=5pt]}] (mot1mot2) to (incomp);
      
      \draw[arrows = {-Stealth[length=5pt]}] (mot1mot3) to (etc);
    \end{tikzpicture}
    \caption{Fonctionnement de l'algorithme de backtracking}
  \end{figure}

\end{document}