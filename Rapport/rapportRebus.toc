\babel@toc {french}{}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Cahier des charges}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Approche adoptée}{3}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Quelques mots de vocabulaire}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}Choix technologiques}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}Langage et bibliothèques du backend}{5}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Langage et bibliothèques du frontend}{5}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Phonétiseur}{6}{subsection.2.3}%
\contentsline {section}{\numberline {3}Développements logiciel}{7}{section.3}%
\contentsline {subsection}{\numberline {3.1}Développement d'une API web}{7}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Développement d'une interface graphique}{8}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Modélisation et remplissage d'une base de données}{14}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Déploiement de l'application}{16}{subsection.3.4}%
\contentsline {section}{\numberline {4}Algorithmes et structures de données}{17}{section.4}%
\contentsline {subsection}{\numberline {4.1}Première approche}{17}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Approche retenue}{18}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Analyse syllabique}{18}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Analyse par phonème}{22}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Approximations}{23}{subsubsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.4}Complexité}{23}{subsubsection.4.2.4}%
\contentsline {section}{\numberline {5}Analyse des résultats}{24}{section.5}%
\contentsline {subsection}{\numberline {5.1}Analyse syllabique}{24}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Analyse par phonèmes}{24}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Approximations}{25}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Mesures}{25}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Pistes d'amélioration}{26}{subsection.5.5}%
\contentsline {section}{\numberline {6}Gestion du projet}{28}{section.6}%
\contentsline {section}{\numberline {7}Bilan et Conclusions}{29}{section.7}%
\contentsline {section}{\numberline {8}Annexe}{30}{section.8}%
\contentsline {subsection}{\numberline {8.1}Liste des rébus test}{30}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Extrait de l'alphabet phonétique utilisé}{31}{subsection.8.2}%
\contentsline {section}{\numberline {9}Webographie}{32}{section.9}%
