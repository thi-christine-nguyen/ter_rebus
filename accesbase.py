from bdd import *

from sqlalchemy import func

# Fonctions de manipulation sur la base de donnée

def getSyllabes(syllabes): 
    """retourne les syllabes si elles existent dans la bd
    
    :param string syllabes: syllables (pa ta te)
    :returns: les syllabes si elles existent, None sinon
    """
    
    try:
        return db.session.query(MotImage.phonemes).filter(MotImage.phonemes==syllabes).one_or_none()
    except :
        return db.session.query(MotImage.phonemes).filter(MotImage.phonemes==syllabes).first()


def getPhonemes(phonemes): 
    """retourne les phonemes s'ils existent dans la bd
    
    :param string phonemes: phonemes 
    :returns: les phonemes si ils existent, None sinon
    """
    
    try:
        return db.session.query(MotImage.phonemes).filter(func.replace(MotImage.phonemes, " ", "") == phonemes).one_or_none()
    except :
        return db.session.query(MotImage.phonemes).filter(func.replace(MotImage.phonemes, " ", "") == phonemes).first()


def getMotBySon(son) : 
    """retourne les mots selon syllabes ou phonemes
    
    :param string son: or di(syllabe) ou ordi(phoneme)
    :returns: les mots si ils existent, None sinon
    """
    
    if ' ' in son : #si c'est des syllabes
        try:
            return db.session.query(MotImage.mot).filter(MotImage.phonemes==son).all()
        except :
            return db.session.query(MotImage.mot).filter(MotImage.phonemes==son).first()
    else : 
        try:
            return db.session.query(MotImage.mot).filter(func.replace(MotImage.phonemes, " ", "")==son).all()
        except :
            return db.session.query(MotImage.mot).filter(func.replace(MotImage.phonemes, " ", "")==son).first()


def getMotsByNbSyllabes (tabSyllabe, nbSyllabes) : 
    """retourne toutes les possibilités de découpage selon un nombre de syllabes
    
    :param list tabSyllabe: tableau contenant toutes les syllabes de la phrase à rébutiser  
    :param int nbSyllabes: le nombre de syllabes max qu'on souhaite avoir pour le découpage
    :returns: un tableau contenant tous les mots possible à nbSyllabes ainsi que leur position dans la phrase
    """
    
    i=0
    j=i+nbSyllabes
    allMotByNbSyllabes  = []
   
    while j <= len(tabSyllabe) : 
        phraseCut = tabSyllabe[i:j]
        phraseCut = ' '.join(map(str,phraseCut))
       
        mot = getSyllabes(phraseCut)
        
        if mot is not None : #si le mot est dans la bdd
            allMotByNbSyllabes.append([i, mot[0].split()])
        i+=1 
        j=i+nbSyllabes
    
    return allMotByNbSyllabes

    
def allMot(phrase, nb) : 
    """retourne tous les mots possible selon la phrase
    
    :param phrase: phrase à rébutiser  
    :param int nb: le nombre max de syllabe à analyser
    :returns: retourne tous les mots possibles ainsi que leur position dans la phrase. 
    """
    
    allMotInPhrase = []
    for i in range(nb, 0, -1) : 
        phoneme = getMotsByNbSyllabes(phrase, i)
        if(phoneme != []) :
            allMotInPhrase += getMotsByNbSyllabes (phrase, i)

    return allMotInPhrase


def getLenPhonemeByMot(mot):
    """retourne le nombre de phonemes du mot

    :param mot: mot dont on cherche le nombre de phonème 
    :returns: retourne le nombre de phonemes
    """

    phon=db.session.query(MotImage.phonemes).filter(MotImage.mot==mot).one_or_none()
    p = phon[0].replace(" ", "")

    return len(p)


def getMotsbyNbPhoneme(phrasePho, nbPhoneme):
    """retourne toutes les possibilités de découpage selon un nombre de phonemes
    :param list phrasePho: chaine de caractère contenant tous les phonemes de la phrase a rebutiser  
    :param int nbPhoneme: le nombre de phonemes max qu'on souhaite avoir pour le découpage
    :returns: un tableau contenant tous les mots possibles à nbPhoneme, le phoneme qui a été construit ainsi que leur position dans la phrase
    """
    
    i=0
    j=i+nbPhoneme
    allMotByNbPhoneme = []
  
    while j <= len(phrasePho) : 
        phraseCut = phrasePho[i:j]    
        mot = getPhonemes(phraseCut)
        
        if mot is not None : #si le mot est dans la bdd
            phoneme = mot[0].replace(" ", "")
            allMotByNbPhoneme.append([i, mot[0].split(), phoneme])
        i+=1 
        j=i+nbPhoneme
    
    return allMotByNbPhoneme


def allMotPhonemes(phrasePho, nb):
    """retourne tous les mots possibles selon la phrase, en ayant découpé selon les phonemes
    
    :param phrase: phrase à rébutiser qui a été phonétisée et mise sous la forme d'une chaine de caracteres 
    :param int nb: le nombre max de phonemes à analyser
    :returns: retourne tous les mots possibles ainsi que leur position dans la phrase. 
    """
    
    allMotInPhrase = []
    for i in range(nb, 0, -1) : 
        phoneme = getMotsbyNbPhoneme(phrasePho, i)
        if(phoneme != []) :
            allMotInPhrase += getMotsbyNbPhoneme(phrasePho, i)
    
    return allMotInPhrase


def getImageFromMot(mots) : 
    """retourne le lien des images de la liste de mots
    :param mots: liste de mot
    :returns: retourne un tableau contenant le lien des images
    """

    tab = []
    if mots == None : 
        return None 
    for m in mots : 
        tab.append(db.session.query(MotImage.image).filter(MotImage.mot==m).one_or_none()[0])
    
    return tab
