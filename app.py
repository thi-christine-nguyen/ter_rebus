from flask import Flask, request, render_template, redirect, url_for, session, flash, jsonify, make_response
from rebutiseur import *
from phonetiseur import *
import secrets
import random
from test import *
from linguistique import *

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'                                # Pour utiliser les sessions.
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///database.db"         # Utilisation d'une bdd SQLite locale.

admin_key = 'zoemelissapablothi'                                        # Pour le mode administrateur.
auth_tokens = ["KLmfs9MdCrckiNsGwZvrvA"]                                # Jetons des utilisateurs connectés

from bdd import db, MotImage, MotAValider, suppressionTables, creationTables, suppressionDonnees, insertionDonnees, insertionMot # Importation des classes et fonctions stockées dans `bdd.py`.

db.init_app(app)        # Lien entre Flask et SQLAlchemy.

# Si nécessaire, décommenter pour la suppression / création des tables :

# with app.app_context():
#     suppressionTables()
#     creationTables()

# Si nécessaire, décommenter pour la suppression / insertion des données de test :

# with app.app_context():
#     suppressionDonnees(app)
#     insertionDonnees(app)

rand = random.randint(1, getNbRebusDuJour("rebusDuJour.json")-1)
rebus_du_jour = rebusDuJour(app, "rebusDuJour.json", rand)
cpt_rebus_du_jour = rand+1


# Si nécessaire, décommenter pour lancer les tests :

# print(testRebus(app, "rebusATester.json"))

# Fonctions pour résoudre les problèmes de CORS (cross origin)

def _build_cors_preflight_response():
    response = make_response()
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Headers", "*")
    response.headers.add("Access-Control-Allow-Methods", "*")
    return response

def _corsify_actual_response(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


# Endpoints de l'API :

@app.route("/")
def index():
    """
    Page d'accueil, affiche l'interface SwaggerUI avec les différents endpoints disponibles.
    """
    return render_template("index.html")


@app.route("/api/openapi.json", methods=['GET', 'OPTIONS'])
def api_openapi():
    """
    Route servant le fichier JSON décrivant les
    différents endpoints de notre API. Ce fichier
    suit les spécifications OpenAPI.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'GET':
        f = open("openapi.json", encoding="utf-8")
        data = json.load(f)
        return _corsify_actual_response(jsonify(data))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/mots', methods=['GET', 'OPTIONS'])
def api_mots():
    """
    Endpoint permettant d'obtenir la liste
    des mots dans la base de données.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'GET':
        motsImages = db.session.query(MotImage).order_by(MotImage.mot).all()
        if "auth-token" in request.headers:
            if request.headers.get("auth-token") in auth_tokens:    # Si l'utilisateur est connecté en mode admin
                motsAValider = db.session.query(MotAValider).order_by(MotAValider.mot).all()
                return _corsify_actual_response(jsonify({"mots":list(mot.mot for mot in motsImages),"mots_a_valider":list(mot.mot for mot in motsAValider)}))
            else:
                return _corsify_actual_response(jsonify({"mots":list(mot.mot for mot in motsImages),"erreur": "Le jeton d'authentification est incorrect ou a expiré."}))
        return _corsify_actual_response(jsonify({"mots":list(mot.mot for mot in motsImages)}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/add_mot', methods=['POST', 'OPTIONS'])
def api_add_mot():
    """
    Endpoint permettant de soumettre un mot à
    la vérification pour l'ajouter dans la bdd.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'POST':
        mot = request.form.get("mot").strip().lower()
        if mot == "":
            return _corsify_actual_response(jsonify({"erreur": "Entrez un mot correct."}))
        if len(phonetise(mot).split(" ")) > 3:
            return _corsify_actual_response(jsonify({"erreur": "Ce mot est trop grand."}))
        if db.session.query(MotImage).filter(MotImage.mot == mot).first() == None and db.session.query(MotAValider).filter(MotAValider.mot == mot).first() == None:
            db.session.add(MotAValider(mot=mot))
            db.session.commit()
            return _corsify_actual_response(jsonify({"succes": "Mot ajouté !"}))
        return _corsify_actual_response(jsonify({"erreur": "Mot déjà ajouté."}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/rebutise', methods=['POST', 'OPTIONS'])
def api_rebutise():
    """
    Endpoint permettant de rébutiser une phrase.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'POST':

        phrase = request.form.get("phrase").strip().lower()
        if phrase == "":
            return _corsify_actual_response(jsonify({"erreur": "Entrez une phrase correcte."}))
        else:
            # Génération du rébus
            images = appelRech(app, phrase, "")

            if images == None :
                return _corsify_actual_response(jsonify({"erreur": "Le rébus n'a pas pu être construit, veuillez changer votre phrase."}))
            else :
                return _corsify_actual_response(jsonify({"succes": "Rébus généré !","mots": phrase.split(" "),"images":images}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/genererrebusdujour', methods=['GET', 'OPTION'])
def generateRebusDuJour():
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()

    if request.method == 'GET':
        if "auth-token" in request.headers:
            if request.headers.get("auth-token") in auth_tokens:
                global rebus_du_jour
                global cpt_rebus_du_jour

                # Générer un nouveau rébus
                total = getNbRebusDuJour("rebusDuJour.json")
                if cpt_rebus_du_jour == total :
                    cpt_rebus_du_jour = 0

                rebus_du_jour = rebusDuJour(app, "rebusDuJour.json", cpt_rebus_du_jour)
                cpt_rebus_du_jour+=1
                return jsonify({"rebus_du_jour": rebus_du_jour})
            else :
                return _corsify_actual_response(jsonify({"erreur": "Le jeton d'authentification est incorrect ou a expiré."}))
        return _corsify_actual_response(jsonify({"erreur": "Vous devez être connecté en mode admin."}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/rebusdujour', methods=['GET', 'OPTIONS'])
def api_rebusdujour():
    """
    Endpoint permettant d'obtenir le rébus du jour.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'GET':
        global rebus_du_jour
        global cpt_rebus_du_jour

        response_data = {"taille": len(rebus_du_jour[0]), "images": rebus_du_jour[0], "hier" : rebus_du_jour[2]}
        return _corsify_actual_response(jsonify(response_data))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/devine', methods=['POST', 'OPTIONS'])
def api_devine():
    """
    Endpoint permettant de soumettre sa réponse
    pour deviner le rébus du jour.
    """
    global rebus_du_jour

    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'POST':
        phrase = request.form.get("phrase").strip().lower()
        phrase = cleanPhrase(phrase)
        r = cleanPhrase(rebus_du_jour[1])

        if phrase == r:
            return _corsify_actual_response(jsonify({"succes": "Bravo, vous avez trouvé le rébus !"}))
        else:
            return _corsify_actual_response(jsonify({"erreur": "Mauvaise réponse."}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/connexion', methods=['POST', 'OPTIONS'])
def api_connexion():
    """
    Endpoint permettant de soumettre un mot
    de passe pour se connecter en mode admin.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'POST':
        mdp = request.form.get("mdp").strip().lower()
        if mdp == admin_key:
            # Génération et ajout d'un jeton d'authentification aléatoire
            auth_token = secrets.token_urlsafe(16)
            auth_tokens.append(auth_token)
            return _corsify_actual_response(jsonify({"succes": "Connecté en mode admin.", "auth-token": auth_token}))
        else:
            return _corsify_actual_response(jsonify({"erreur": "Mot de passe incorrect."}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/valider/<mot>', methods=['GET', 'OPTIONS'])
def api_valider_mot(mot):
    """
    Endpoint permettant de valider un mot soumis par un
    utilisateur, accessible uniquement par les administrateurs.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'GET':
        if "auth-token" in request.headers:
            if request.headers.get("auth-token") in auth_tokens:
                motAValider = db.session.query(MotAValider).filter(MotAValider.mot == mot).first()
                motImage = db.session.query(MotImage).filter(MotImage.mot == mot).first()
                if motImage != None:
                    db.session.query(MotAValider).filter(MotAValider.mot == mot).delete()
                    db.session.commit()
                if motAValider != None:
                    db.session.query(MotAValider).filter(MotAValider.mot == mot).delete()
                    db.session.commit()
                    insertionMot(mot, "")
                return _corsify_actual_response(jsonify({"succes": "Le mot a bien été validé !"}))
            else:
                return _corsify_actual_response(jsonify({"erreur": "Le jeton d'authentification est incorrect ou a expiré."}))
        return _corsify_actual_response(jsonify({"erreur": "Vous devez être connecté en mode admin."}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/refuser/<mot>', methods=['GET', 'OPTIONS'])
def api_refuser_mot(mot):
    """
    Endpoint permettant de refuser un mot soumis par un
    utilisateur, accessible uniquement par les administrateurs.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'GET':
        if "auth-token" in request.headers:
            if request.headers.get("auth-token") in auth_tokens:
                motAValider = db.session.query(MotAValider).filter(MotAValider.mot == mot).first()
                if motAValider != None:
                    db.session.query(MotAValider).filter(MotAValider.mot == mot).delete()
                    db.session.commit()
                return _corsify_actual_response(jsonify({"succes": "Le mot a bien été refusé !"}))
            else:
                return _corsify_actual_response(jsonify({"erreur": "Le jeton d'authentification est incorrect ou a expiré."}))
        return _corsify_actual_response(jsonify({"erreur": "Vous devez être connecté en mode admin."}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/supprimer/<mot>', methods=['GET', 'OPTIONS'])
def api_supprimer_mot(mot):
    """
    Endpoint permettant de supprimer un mot de la base de données,
    accessible uniquement par les administrateurs.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'GET':
        if "auth-token" in request.headers:
            if request.headers.get("auth-token") in auth_tokens:
                motImage = db.session.query(MotImage).filter(MotImage.mot == mot).first()
                if motImage != None:
                    db.session.query(MotImage).filter(MotImage.mot == mot).delete()
                    db.session.commit()
                return _corsify_actual_response(jsonify({"succes": "Le mot a bien été supprimé !"}))
            else:
                return _corsify_actual_response(jsonify({"erreur": "Le jeton d'authentification est incorrect ou a expiré."}))
        return _corsify_actual_response(jsonify({"erreur": "Vous devez être connecté en mode admin."}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/modifier', methods=['POST', 'OPTIONS'])
def api_modifier():
    """
    Endpoint permettant de modifier un mot et son
    image dans la base de données.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'POST':
        if "auth-token" in request.headers:
            if request.headers.get("auth-token") in auth_tokens:
                ancienmot = request.form.get("ancienmot").strip().lower()
                nouveaumot = request.form.get("nouveaumot").strip().lower()
                image = request.form.get("image").strip()
                if db.session.query(MotImage).filter(MotImage.mot == ancienmot).first() == None:
                    return _corsify_actual_response(jsonify({"erreur": "Ce mot n'existe pas."}))
                if nouveaumot == "":
                    return _corsify_actual_response(jsonify({"erreur": "Entrez un mot correct."}))
                db.session.query(MotImage).filter(MotImage.mot == ancienmot).delete()
                phonemes = phonetise(nouveaumot)
                db.session.add(MotImage(mot=nouveaumot,image=image,phonemes=phonemes))
                db.session.commit()
                return _corsify_actual_response(jsonify({"succes": "Mot modifié !"}))
            else:
                return _corsify_actual_response(jsonify({"erreur": "Le jeton d'authentification est incorrect ou a expiré."}))
        return _corsify_actual_response(jsonify({"erreur": "Vous devez être connecté en mode admin."}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/image/<mot>', methods=['GET', 'OPTIONS'])
def api_image_mot(mot):
    """
    Endpoint permettant d'obtenir l'image
    du mot dans la base de données.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'GET':
        motImage = db.session.query(MotImage).filter(MotImage.mot == mot).first()
        if motImage == None:
            return _corsify_actual_response(jsonify({"erreur": "Ce mot n'existe pas."}))
        return _corsify_actual_response(jsonify({"image": motImage.image}))
    return {"erreur": "Méthode non supportée."}


@app.route('/api/telecharger', methods=['GET', 'OPTIONS'])
def api_telecharger():
    """
    Endpoint permettant de télécharger une sauvegarde de la
    base de données (mots + images + phonèmes) en JSON.
    """
    if request.method == 'OPTIONS': # CORS preflight
        return _build_cors_preflight_response()
    if request.method == 'GET':
        motsImages = db.session.query(MotImage).all()
        response = jsonify({"mots":list(motImage.json() for motImage in motsImages)})
        response.headers.add("Content-Disposition", "attachment; filename=\"motsimages.json\"")
        return _corsify_actual_response(response)
    return {"erreur": "Méthode non supportée."}


if __name__ == '__main__':                              # Permet de lancer l'app par la commande `python3 app.py`,
    app.run(host='0.0.0.0', port=5000, debug=True)      # sans utiliser `flask run`. Utilisation du debugger.
