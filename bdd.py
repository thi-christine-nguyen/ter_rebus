from flask_sqlalchemy import SQLAlchemy
from phonetiseur import phonetise
from image_wikimedia import getWikimediaImageFor
import json


db = SQLAlchemy()

# Tables SQL sous forme de classes, héritant de `db.Model`.

class MotImage(db.Model):
    mot = db.Column(db.String(200), primary_key=True)       # Une colonne de la table = un attribut de la classe
    image = db.Column(db.Text(), nullable=False)
    phonemes = db.Column(db.String(200), nullable=False)

    def __repr__(self):                                     # Méthode pour représenter l'objet sous forme de string
        return '<MotImage %s>' % self.mot
    
    def json(self):                                         # Méthode pour représenter l'objet en format json
        return {
            "mot": self.mot,
            "image": self.image,
            "phonemes": self.phonemes
        }

class MotAValider(db.Model):
    mot = db.Column(db.String(200), primary_key=True)

    def __repr__(self):
        return '<MotAValider %s>' % self.mot
    
    def json(self):
        return {
            "mot": self.mot
        }


def suppressionTables():
    """
    Suppression des tables de la bdd.
    """
    print("Suppression des tables")
    db.drop_all()


def creationTables():
    """
    Création des tables de la bdd.
    """
    print("Création des tables")
    db.create_all()


def suppressionDonnees(app):
    """
    Suppression des données de test de la bdd.
    """
    print("Suppression des données")
    with app.app_context():
        db.session.query(MotImage).delete()
        db.session.query(MotAValider).delete()
        db.session.commit()


def insertionDonnees(app):
    """
    Insertion des données de test dans la bdd.
    """
    print("Insertion des données")
    
    insertionDepuisJSON("mots_db/motsimages.json")


def insertionMot(mot, image):
    """
    Insertion d'un mot, de son image et de ses
    phonèmes associés dans la base de données.

    :param mot: Le mot à insérer
    """
    phonemes = phonetise(mot)                                                           # Utilisation de l'algorithme de phonétisation
    if len(phonemes.split(" ")) > 3:                                                    # Limitation à 3 phonèmes max par mot
        return
    
    if image == "": 
        image = getWikimediaImageFor(mot)                                               # Ajoute une image si le mot n'en a pas déjà une 
    if db.session.query(MotImage).filter(MotImage.mot == mot).first() is None:          # Si le mot n'est pas déjà présent
        db.session.add(MotImage(mot=mot, image=image, phonemes=phonemes))               # Ajout du MotImage correspondant    
        db.session.commit()
        print("Ajout de <MotImage mot=" + mot + ", image=" + image[:80] + "..., phonemes=" + phonemes + ">")


def insertionDepuisJSON(chemin):
    """
    Insertion dans la base de données d'une liste
    de mots contenue dans un fichier JSON.

    :param chemin: Le chemin du fichier à charger
    """
    f = open(chemin, encoding="utf-8")                                                  # Ouverture du fichier
    data = json.load(f)                                                                 # Chargement des données dans l'objet data
    for mots in data['mots']:   
        mot = mots['mot']
        mot = mot.encode('utf-8').decode('utf-8')                                       # Encodage des caractères spéciaux 
        image = mots["image"]                             
        insertionMot(mot, image)                                                     
    f.close()                                                                           # Fermeture du fichier
