import requests

auth_token = "KLmfs9MdCrckiNsGwZvrvA"
#url = "https://terrebus.eu.pythonanywhere.com/api/genererrebusdujour"
url = "http://127.0.0.1:5000/api/genererrebusdujour"

headers = {
    "auth-token": auth_token
}

response = requests.get(url, headers=headers)


if response.status_code == 200:
    print("Rébus du jour généré avec succès !")
else:
    print("Erreur lors de la génération du rébus du jour : ", response.status_code)
