import requests
from bs4 import BeautifulSoup

def getWikimediaImageFor(word):
    """
    Recherche une image correspondante au
    mot donné sur Wikimedia Commons.

    :param mot: Mot à rechercher
    :return: Lien vers l'image, vide si non trouvée
    :rtype: str
    """
    htmldata = requests.get("https://commons.wikimedia.org/w/index.php?search=" + word + "&title=Special:MediaSearch&go=Lire&uselang=fr&type=image").text # Récupération de la page web
    soup = BeautifulSoup(htmldata, 'html.parser') # Création d'un objet correspondant au DOM de la page
    img = soup.find_all('img')[0] # Récupération du premier élément de type image
    if img['alt'] == "":
        return ""
    return img['src']
