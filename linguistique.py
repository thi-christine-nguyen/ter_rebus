import re


def cleanPhrase(text):
    # Supprimer les guillemets doubles et simples
    text = re.sub(r'[\'´’`"]+', '', text)

    # Remplacer par des espaces
    text = re.sub(r'[-:.,]', ' ', text)

    # Convertir en minuscules
    text = text.lower()
    # Supprimer les espaces en double
    text = ' '.join(text.split())
    return text

