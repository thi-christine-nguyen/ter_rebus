import subprocess

def phonetise(mot):
    """
    Cette fonction phonétise un mot à l'aide
    du programme perl de phonétisation.

    :param mot: Mot à phonétiser
    :type mot: str
    :returns: La liste des phonèmes composant le mot
    :rtype: list[str]
    """
    commande = "perl phonetisation_mot_v3.pl " + mot # Commande pour lancer le script perl sur le mot en paramètre
    processus = subprocess.Popen(commande.split(), cwd="phonetiseur/", stdout=subprocess.PIPE) # Crée un sous-processus qui lance la commande
    output = processus.communicate()[0] # Récupère la sortie standard
    phonemes = output.decode('Latin1').strip() # Conversion de bytes vers string et suppression des espaces en trop
    return phonemes
