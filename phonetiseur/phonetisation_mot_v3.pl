#!/usr/bin/env perl

#use utf8;
use Encode qw(decode encode);

## phonetisation_mot_v3.pl (+variantes graphiques SMS)
## Phonétise un mot passé en paramètre (et en UTF8)
## Regénère les variantes graphiques liées à la phonétisation produite

## Pierre Pompidor
## 7 décembre 2012

## En paramètre(s) :
#     mot à pĥonétiser                                    : phonetisation_mot_v3.pl mot
#     mot à pĥonétiser avec variantes graphiques simples  : phonetisation_mot.pl_v3 *mot
#     mot à pĥonétiser avec variantes graphiques étendues : phonetisation_mot_v3.pl **mot
#     mot à pĥonétiser avec variantes graphiques SMS      : phonetisation_mot_v3.pl +mot


## En lecture :

## règles de phonétisation : francais_comparatif_prononciations
#  contraintes d'application des règles :
   #  . rien
   #  * voyelle
   #  % consonne
   #  [tv] t ou v (exemple)
   #  [^tv] ni t, ni v (exemple)
   #  + liaison
   #  - coupure
   #  |préfixe
   #  suffixe| terminal ou complété de "s"
   #  FORME_VERBALE(phonemes)

## formes verbales
#  pour connaître la prononciation d'un mot se finissant par ent ou tions 


my $voyelle   = "[aeiouàâéèêëîïôöùûüy]";
my $consonne  = "[bcçdfghjklmnpqrstvwxz]";
my $consonne2 = "[çjlrqw]";

$basename = "RESSOURCES";
my $mot_parametre = shift or die ("mot manquant");
 
$mot_parametre = decode("utf-8", $mot_parametre);
# décommenter pour windows :
# $mot_parametre = decode("iso-8859-1", $mot_parametre);

sub cartesian {
       my $len = 1;
       my (@ret,$rep,$i,$j,$p,$k);
   
       for (@_) { $len *= @$_ }
   
       for ($rep = 1, $i = 0; $i < @_; $rep *= @{ $_[$i] }, $i++) {
         for ($j = 0, $p = 0; $j < $len; $j += $rep, $p++) {
           for ($k = 0; $k < $rep; $k++) {
             print STDERR << "DEBUGGING" if 0;  # set to true to see debug output
   repetition value: $rep
   modifying set[@{[ $j + $k]}], index[$i]
   value is element @{[ $p % @{ $_[$i] } ]} ('$_[$i][$p % @{ $_[$i] }]') of original set[$i]
   
DEBUGGING
             $ret[$j + $k][$i] = $_[$i][$p % @{ $_[$i] }]
           }
         }
       }
   
       return @ret;
     }


sub variantes {
    $phonetiques = shift;
    chop $phonetiques;
    $phonetiques =~ tr/|+-.//d;
    # print "\n********** Dans variantes avec $phonetiques **********\n";

    my $nb_variantes = 0;
    my $liste_raccourcis = "";  # liste des suites de phonèmes raccourcis sur une lettre

    if (   ($variantes == 3 && open FD, "<:encoding(utf-8)", "francais_phonemes_2_SMS") 
        || ($variantes == 1 && open FD, "<:encoding(utf-8)", "francais_variantes_graphiques_simplifiees") 
        || ($variantes == 2 && open FD, "<:encoding(utf-8)", "francais_variantes_graphiques")) {
        my @variantes_graphiques = <FD>;
        foreach my $ligne (@variantes_graphiques) {
            #print "\n$ligne";
            #$ligne =~ s/\[[^]]+\]//g;
            if ($ligne =~ /^([^#][^ ]+) : (.+)/) {
                my $phonemes = $1;
                my $variantes = $2;
                if ($variantes =~ /(.+)\(/) { $variantes = $1; }  # pour supprimer d'éventuels commentaires
                $variantes_sur_trois_phonemes{$phonemes} = $variantes;
                $liste_raccourcis .= $variantes;
            }
            elsif ($ligne =~ /^([^#][^ ]) : (.+)/) {
                my $phonemes = $1;
                my $variantes = $2;
                if ($variantes =~ /(.+)\(/) { $variantes = $1; }  # pour supprimer d'éventuels commentaires
                $variantes_sur_deux_phonemes{$phonemes} = $variantes;
                $liste_raccourcis .= $variantes;
            }
            elsif ($ligne =~ /^([^#]) : (.+)/) {
                my $phonemes = $1;
                my $variantes = $2;
                if ($variantes =~ /(.+)\(/) { $variantes = $1; }  # pour supprimer d'éventuels commentaires
                #print $ligne;
                $variantes{$phonemes} = $variantes;
            }
        }

        if ($variantes == 1 || $variantes == 2) {
           #print "Liste des raccourcis : $liste_raccourcis\n";	
           recherche_variantes_graphiques($phonetiques, $liste_raccourcis);
           foreach my $variante (keys %variantes_sur_deux_phonemes) {
              my $raccourci = $variantes_sur_deux_phonemes{$variante};
              if ($phonetiques =~ s/$variante/$raccourci/g) {
                #print "-> $variante : appel de $phonetiques\n";
                recherche_variantes_graphiques($phonetiques, $liste_raccourcis);
              }
          }
        }
        if ($variantes == 3) {
           recherche_SMS($phonetiques);
        }
    }
    else {
        printf "\n -> la liste des variantes graphiques n'a pas pu être ouverte !\n";
    }
}


sub recherche_SMS() {
   my $phonetiques = shift;

   foreach my $variante (keys %variantes_sur_trois_phonemes) {
      if ($phonetiques =~ s/$variante/$variantes_sur_trois_phonemes{$variante}/g) { #  print "\t $variante\n";
      }
   }
   foreach my $variante (keys %variantes_sur_deux_phonemes) {
      if ($phonetiques =~ s/$variante/$variantes_sur_deux_phonemes{$variante}/g) { #  print "\t $variante\n";
      }
   }
   foreach my $variante (keys %variantes) {
      if ($phonetiques =~ s/$variante/$variantes{$variante}/g) { # print "\t $variante\n";
      }
   }
   $phonetiques =~ s/j§/ion/g;  # ion
   $phonetiques =~ s/§/on/g;    # ion
   $phonetiques =~ s/@/an/g;    # an
   $phonetiques =~ s/S/ch/g;    # ch
   $phonetiques =~ s/://g;      # suppression du e muet
   $phonetiques =~ s/ //g;      # ...
   print "$phonetiques\n";; 
}


sub recherche_variantes_graphiques() {
   my $phonetiques = shift;
   my $liste_raccourcis = shift;

   undef @liste_homophones;

   # Recherches des variantes graphiques
   #print "\n\nRecherches des variantes graphiques sur $phonetiques ($liste_raccourcis) :\n";
   my $num_phoneme = 1;
   my $nb_phonemes = my @phonemes = split //, $phonetiques;
   foreach my $phoneme (@phonemes) {

       if ($phoneme !~ /$liste_raccourcis/) {
            my @variantes = split / +/, $variantes{$phoneme};
            my $num_element = 0;
            my @elements_a_effacer;
            foreach my $variante (@variantes) {
                $variante =~ s/^\[[^]]+\]//g;  # Pour l'instant élimination des pré-conditions
                if ($variante =~ /(.+?)\[/) {
                    my $homophone = $1;
                    my @contraintes = $variante =~ /\[(.+?)\]/g;
                    $variante =~ s/\[.+\]//g;
                    foreach $condition (@contraintes) {
                       $contraintes_variantes{$homophone} .= "$condition ";

                       # Vérification de la contrainte
                       if ($condition eq "notD" && $num_phoneme == 1) {
                          #print "   -> Contrainte violée pour $homophone ($num_element) : notD !";
                          push @elements_a_effacer, $num_element;
                       }

                       elsif ($condition eq "F" && $num_phoneme != $nb_phonemes) {
                          #print "\n   -> Contrainte violée pour $homophone ($num_element) : F !";
                          push @elements_a_effacer, $num_element;
                       }

                       elsif ($condition eq "FouC" && (   $num_phoneme != $nb_phonemes
                                                       && $phonemes[$num_phoneme] !~ /$consonne/))
                       {
                          #print "\n   -> Contrainte violée pour $homophone ($num_element) $phonemes[$num_phoneme] : FouC !";
                          push @elements_a_effacer, $num_element;
                       }
                       #elsif ($condition) { # [condition]

                       #}
                    }
                }
                $num_element++;
            }
            # print "\n   Liste des éléments à effacer :";
            my $nb_elt_effaces = 0;
            foreach my $elt (@elements_a_effacer) {
               # print " $elt";
               splice @variantes, $elt-$nb_elt_effaces, 1;
               $nb_elt_effaces++;
            }
            #print "\n";

            #Si tous les éléments ont été effacés, on réinjecte le phonème
            if (@variantes == 0) { push @variantes, $phoneme; }
            push @liste_homophones, [@variantes];

          }
          else {
            #print "protection de $phoneme\n";
            push @liste_homophones, [$phoneme];
          }
          
            $num_phoneme++;
        }

        my @variantes = map "@$_\n", cartesian( @liste_homophones );

        # Affichage de toutes les variantes graphiques
        # Si mode SMS, filtre de celle qui est la plus courte !
        #print "\n\nVariantes graphiques :\n";
        my $num_variante = 0;
        foreach my $variante (@variantes) {
            $chaine = join "", $variante;
            $chaine =~ tr/ //d;
            chomp $chaine;
            print "$chaine#";
            $num_variante++;
        }

        # Affichage des conditions sur les variantes graphiques
        #print "\nConditions sur les variantes graphiques :\n";
        foreach my $variante_contrainte (keys %contraintes_variantes) {
            #print "   $variante_contrainte : $contraintes_variantes{$variante_contrainte}\n";
        }
}


sub formes_verbales {
   if (open FD, "<:encoding(utf-8)", "francais_formes_verbales") {
       my $nb_formes_verbales = @formes_verbales = <FD>;
       chomp @formes_verbales;
       #print "\n -> lecture de $nb_formes_verbales formes verbales (ex. en 'ent' ou 'tions')";
       close FD;
   }
   else {
       print "\n -> la liste des formes verbales n'a pas pu etre ouverte !";
   }
}


sub prefixes {
   if (open FD, "<:encoding(utf-8)", "francais_prefixes") {
       my $nb_prefixes = my @prefixes = <FD>;
       chomp @prefixes;
       foreach my $prefixe (@prefixes) {
            $prefixes_du_francais{$prefixe} = 1;
       }
       #print "\n -> lecture de $nb_prefixes prefixes";
       close FD;
   }
   else {
       print "\n -> la liste des prefixes n'a pas pu etre ouverte !";
   }
}


sub phonetisation {
    my $mot  = $_[0];
    my $mode = $_[1]; # Affichage de la trace
    my $g    = $_[2]; # Caractère précédant le mot analysé
                      # Dans le cas de la phonétisation d'une partie d'un mot composé
                      #      dernière lettre du mot précédent pour assurer la liaison
                      #      exemple : sous-entendu  -> su z@ t@ dy
                      # sinon <

    my $copie_mot = $mot;


    ## PREPROCESSING :
    # les y sont réécrits en "ii" si ils sont suivis d'une voyelle
    $mot_initial = $mot;
    $mot =~ s/y($voyelle)/ii$1/g;

    my $idefix_phonetisation = "";
    my $suffixe_phonetisation = "";

    if ($mode) { print "\n\nPhonetisation de $mot (g=$g): "; }

    #print "**** Exceptions :<br/>";
    # s facultatif à la suite du candidat
    my $mot_sans_s = "";
    if ($mot =~ /(.+)s$/) { $mot_sans_s = $1; }

    if (exists $mots{$mot}) {
        $idefix_phonetisation = "|".$mots{$mot}."|";
        goto fin;
    }
    elsif ($mot_sans_s ne "" && exists $mots{$mot_sans_s}) {
        $idefix_phonetisation = "|".$mots{$mot_sans_s}."|";
        goto fin;
    }
    else {
        # recherche d'une forme verbale construite sur une exception
        for my $forme (keys %formes_exceptionnelles) {
           if ($mot =~ /^$forme(.+)/) {
               if ($mode) { print "\n -> $forme/$mot sur une forme exceptionnelle"; }
               $mot = $1;
               $g .= $forme;
               $idefix_phonetisation = $formes_exceptionnelles{$forme}."|";
               goto fin_exceptions;
           } 
        }
    }
    fin_exceptions:;

    #print "**** Suffixes :<br/>";
    # testés si ils couvrent toute la chaîne de caractère restante
    # s facultatif à la suite du candidat
    foreach my $taille (reverse sort keys %suffixes) {
	foreach my $elem (@{$suffixes{$taille}}) {
           my $motif = @$elem[0];
           my $phonemes = @$elem[1];       
           my $l = @$elem[2]; my $r = @$elem[3];

           if ($mot =~ /^${motif}s?$/) {
                                 # pour rajouter un 's' au suffixe

               if ($l ne "" and $g ne "^") {
                   #print "\n$motif : vérification g=$g vs l=$l";
                   if ($g !~ /$l$/) { #print "négative";
                                     goto fin_boucle_suffixes; }
                   #print " positive";
               }
 
               if ($phonemes =~ /FORME_VERBALE\((.*)\)/) {
                   if ($mode) { print " -> recherche d'une forme verbale identique"; }
                   $phonemes = $1;
                   foreach my $forme (@formes_verbales) {
                      if ($forme eq $mot_initial) {
                          $suffixe_phonetisation = $phonemes;
                          if ($mode) { print "\n   -> OK ($forme) : $phonemes"; }
                          goto fin;
                      }
                   }
               }
               else {
                    $suffixe_phonetisation = $phonemes;
                    if ($mode) { print "\n -> $motif en suffixe : $phonemes, "; }
                    goto fin;
               }
           }
           fin_boucle_suffixes:;
       }
    }
    fin_suffixes:;



    #print "**** Idefix :\n";
    my $trouve = 0;
    foreach my $taille (reverse sort keys %idefix) {
	foreach my $elem (@{$idefix{$taille}}) {
                my $motif = @$elem[0];
                #print "\n$motif";
                my $phonemes = @$elem[1];
                my $l = @$elem[2]; $r = @$elem[3];
                # if ($motif =~ /^s$/) { print "\n ***** $motif/$mot avec l=$l et r=$r"; }

                # Si le motif est conditionné par une consonne en partie droite (%),
                # cette consonne peut être interchangée avec une fin de valeur
                if ($r eq $consonne && $mot =~ /^${motif}$/) {
                    if ($mode) { print "\n -1-> $motif/$& en idefix avec l=$l et r=$r : $phonemes"; }
                        if ($l ne "" and $g ne "<") {  # Condition à vérifier à gauche
                        if ($mode) { print " : vérification $g vs $l"; }
                        if ($g !~ /$l$/) { goto fin_boucle_idefix; }
                        if ($mode) { print " positive"; }
                    }
                    $idefix_phonetisation .= $phonemes."|";
                    goto fin;
                }
                
                if ($l eq "prefixe") {
                    $g =~ /<?(.+)/;
                    my $prefixe_a_tester = $1;
                    # print "\n -> Recherche du préfixe $prefixe_a_tester par rapport à $phonemes";
                    if ($mot =~ /^${motif}(.*)/) {
                        if (exists $prefixes_du_francais{$prefixe_a_tester}) {
                            $mot = $1;
                            $idefix_phonetisation .= $phonemes."|";
                            $g .= ${motif};
                            if ($mode) { print " -> préfixe $prefixe_a_tester par rapport à $phonemes : il reste $mot a traiter"; }
                            if ($mot eq "") { goto fin; }
                            goto fin_exceptions;
                         }
                    }
                }

                elsif ($mot =~ /^${motif}(${r}.*)/) {
                    $d = $1;
                    if ($mode) { print "\n -2-> $motif/$mot en idefix avec l=$l (g=$g) et r=$r ($d): $phonemes"; }
                    if ( $l eq "|" && $g ne "<" ) {
                       if ($mode) { print " : vérification préfixe négative"; }
                       goto fin_boucle_idefix;
                    }
                    # if ($l eq "[Z]" and $g eq "<") {  # Liaison dans le cas d'un mot composé (test sur le "s")
                    #     goto fin_boucle_idefix;
                    # }

                    if ($l ne "" and $l ne "<" and $g eq "<") {
                       goto fin_boucle_idefix;
                    }
         
                    if ($l ne "" and $g ne "<") {  # Condition à vérifier à gauche
                        if ($mode) { print " : vérification $g vs $l"; }
                        if ($g !~ /$l$/) { goto fin_boucle_idefix; }
                        if ($mode) { print " positive"; }
                    }
                    $mot = $1;
                    $idefix_phonetisation .= $phonemes."|";
                    $g .= ${motif};
                    if ($mode) { print " -> il reste $mot a traiter"; }
                    if ($mot eq "") { goto fin; }
                    goto fin_exceptions;
		}
                fin_boucle_idefix:;
        }
    }

    fin:;

    return $idefix_phonetisation.$suffixe_phonetisation;
}


sub syllabation {
    my $syllabation = shift;
    my $cesures     = shift;    # Pour faire apparaître les césures (1) ou non (0)

    $syllabation =~ s/\.//g;
    $syllabation =~ s/\+\|//g;   # Pour créer des liaisons
    $syllabation =~ s/\|\+//g;   # Pour créer des liaisons
    $syllabation =~ s/\|\|/ /g;  # Pour insérer des césures
    $syllabation =~ s/\|/ /g;    # Pour insérer des césures
    $syllabation =~ s/-/ /g;     # Pour insérer des césures
    $syllabation =~ tr/\+//d;
    $syllabation =~ s/ +$//g;     # Pour enlever les espaces pendants

    # PATCHES
    #print $syllabation, "\n";
    # serpillère & co : sERpi je R => sER pi je R
    $syllabation =~ s/eR([cdfgkZkmnpstv])/eR $1/g;
    # boulangerie & co : bu l@ Z:Ri => bu l@ Z: ri
    $syllabation =~ s/([lnpstvZ]):([R])/$1: $2/g;
    #print $syllabation, "\n";

    unless ($cesures) {$syllabation =~ s/ //g; } # Pour enlever les césures ;-)

    if ($concatenation) {$syllabation =~ s/(.+) (.)$/$1$2/;}

    return $syllabation;
}


#print "\nChargement des ressources";

if (open FD, "<:encoding(utf-8)", francais_exceptions_phonetiques_brutes) {

    #print "\n -> chargement des exceptions phonétiques : ";

    my %apports_exogenes;

    my @mots = <FD>;
    chomp @mots;
    my $nb_exceptions = 0;
    my $nb_formes_exceptionnelles = 0;
    foreach my $ligne (@mots) {
       if ($ligne =~ /(\S+)\(suffixe\)\s+(\S+)/) {
          my $forme = $1;
          my $prononciation = $2;
          $forme =~ s/y([aeéiou])/ii$1/;
          $formes_exceptionnelles{$forme} = $prononciation;
          $nb_formes_exceptionnelles++;
       }
       elsif ($ligne =~ /(\S+)\s+(\S+)/) {
          my $exception = $1;
          my $prononciation = $2;
          if ($exception =~ s/y([aeéiou])/ii$1/) {
             #print " -> $exception\n";
          }
          $mots{$exception} = $prononciation;

          $nb_exceptions++;
       }  
    }
    close FD;
    #print $nb_exceptions+$nb_formes_exceptionnelles, " dont ", $nb_formes_exceptionnelles, " formes suffixées";
 
    if ($afficher_apports_exogenes) {
    foreach my $apport_exogene (sort {$apports_exogenes{$b} <=> $apports_exogenes{$a}} keys %apports_exogenes) {
       my $pourcentage = $apports_exogenes{$apport_exogene}/$nb_apports_exogenes;
       $pourcentage = sprintf("%.3f", $pourcentage);
       #print "\n   -> $apport_exogene : $apports_exogenes{$apport_exogene} -> $pourcentage";
       }
    }
}


if (open FD, "<:encoding(utf-8)", francais_comparatif_prononciations) {

    #print "\n -> analyse des regles de phonetisation : ";

    my @motifs = <FD>;
    chomp @motifs;
    my $num_ligne = 1;
    my $nb_regles = 0;
    foreach my $ligne_motif (@motifs) {
       if ($ligne_motif !~ /^#/) {  # Pour sauter les lignes commentées
       if ($ligne_motif =~ /(\S+)\s+(\S+)/) {
           $motif = $1;
           $liste_phonemes = $2;
           
           # Contrôle des voyelles ou consonnes en bord gauche ou droit
           $l = ""; $r = "";

           if    ($motif =~ s/\(prefixe\)(.+)$/$1/)       { $l = "prefixe"; }
           elsif ($motif =~ s/\[(.+)\](.+)\*$/$2/)        { $l = "[$1]"; $r = $voyelle; }
           elsif ($motif =~ s/\[(.+)\](.+)%$/$2/)         { $l = "[$1]"; $r = $consonne; }

           elsif ($motif =~ s/^\*(.+)\*/$1/)              { $l = $voyelle; $r = $voyelle; }
           elsif ($motif =~ s/^\*(.+)\[(.+)\]/$1/)        { $l = $voyelle;  $r = "[$2]"; }
           elsif ($motif =~ s/^\*(.+)/$1/)                { $l = $voyelle; }

           elsif ($motif =~ s/^%#(.+)\[(.+)\]/$1/)        { $l = "$consonne$consonne2"; $r = "[$2]"; }
           elsif ($motif =~ s/^%(.+)\[(.+)\]/$1/)         { $l = $consonne; $r = "[$2]"; }
           elsif ($motif =~ s/^%(.+)/$1/)                 { $l = $consonne; }

           elsif ($motif =~ s/(.+)\*$/$1/)                { $r = $voyelle; }
           elsif ($motif =~ s/([^[]+)\[(.+)\]%$/$1/)      { $r = "[$2]$consonne"; }
           elsif ($motif =~ s/(.+)%$/$1/)                 { $r = $consonne; }
 
           elsif ($motif =~ s/^\[(.+)\](.+?)\[(.+)\]/$2/)  { $l = "[$1]"; $r = "[$3]"; }
           elsif ($motif =~ s/^\[(.+)\](.+)/$2/)         { $l = "[$1]"; }
           elsif ($motif =~ s/^([^[]+)\[(.+)\]/$1/)      { $r = "[$2]"; }

           if ($motif =~ /^\|(.+)/) {
                my $long = length($1);
                if (not $idefix{$long}) { $idefix{$long} = (); }
                push @{$idefix{$long}}, ([$1, $liste_phonemes, "<", $r]);
           }
           elsif ($motif =~ /(.+)\|$/) {
                my $long = length($1);
                if (not $suffixes{$long}) { $suffixes{$long} = (); }
                push @{$suffixes{$long}}, ([$1, $liste_phonemes, $l, $r]);
           }
           else {
                my $long = length($motif);
                #print "\n$motif ($long) : $liste_phonemes";
                if (not $idefix{$long}) { $idefix{$long} = (); }
                push @{$idefix{$long}}, ([$motif, $liste_phonemes, $l, $r]);
           }
           $nb_regles++;
       }
       else {
           print "Problème ($num_ligne) : $ligne_motif\n";
       }
       }
       $num_ligne++;
   }
   #print $nb_regles;

   formes_verbales();
   prefixes();

   $variantes = 0;
   $concatenation = 0;
   if ($mot_parametre =~ /\+(.+)/) {
            $mot_parametre = $1;
            $variantes = 3;       # Variantes SMS
   }
   elsif ($mot_parametre =~ /\*\*(.+)/) {
            $mot_parametre = $1;  # variantes graphiques étendues
            $variantes = 2;
   }
   elsif ($mot_parametre =~ /\*(.+)/) {
            $mot_parametre = $1;
            $variantes = 1;       # variantes graphiques simples
   }
    elsif ($mot_parametre =~ /\-(.+)/) {
            $mot_parametre = $1;
            $concatenation = 1;   # concaténation de la consonne pendante avec la dernière syllabe
   }
        # les mots comprenants des - sont découpés :
        my @mots = split /-/, $mot_parametre;
        my $phonetiques = "";
        $dlettre = "<";   # Marqueur du début d'un mot
        foreach my $mot (@mots) {
           $mot =~ tr/A-Z/a-z/;  # normalisation en minuscules
           my $phonetique = phonetisation($mot, 0, '<');  #$dlettre);  # le second paramètre est un booléen permettant d'activer la trace
#$phonetique =~ s/[|+.]//g;
           # print "$phonetique\n";
           $phonetiques .= "$phonetique-";
           $dlettre = chop($mot);
           # Gestion des liaisons dans un mot composé : moche, à revoir
           # if ($dlettre eq "s") {
           #     $dlettre = 'Z';
           # }
        }

        my $syllabation = syllabation($phonetiques, 1);
        print "$syllabation";
        if ($variantes) { variantes($phonetiques); }
}
else {
    print ."Le fichier de motifs de prononciations n'a pas ete trouve !<br/>";
}

