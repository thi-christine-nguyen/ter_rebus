#!/usr/bin/env perl

use encoding 'utf8';
use utf8;
use Encode qw(decode encode);


if (open FD, "<:encoding(utf-8)", "tests_francais_SMS") {
    my @tests_SMS = <FD>;
    chomp @tests_SMS;
    foreach my $ligne (@tests_SMS) {
       # print "$ligne\n";
       if ($ligne =~ /(.+) : (.+)/) {
           my $mot = $1;
           my $SMS_test = $2;
           print "$mot : ";
           my $SMS = `phonetisation_mot_v3.pl +$mot`;
           if ($SMS =~ /(.+)\n(.+)/) {
              my $phonetique = $1;
              my $SMS = $2;
              print "$SMS_test / $SMS\n";
           }
       }
    }
    close FD;
}
else {
    print "\n -> la liste des SMS à tester n'a pu être ouverte !";
}
