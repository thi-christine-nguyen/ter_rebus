from phonetiseur import phonetise
from accesbase import *
import copy
import math
import time
from linguistique import *

MAXSYL = 3
MAXPHO = 10
APPROX = {"iau" : " i au ", "ier" : "i er", "ien" : "i un", "gni":"ni ", "gn":"ni ", "ia":" i a"}


def phrase_son(phrase): 
    """transforme une phrase en tableau de syllabes
    
    :param string phrase: phrase à rébutiser 
    :returns: un tableau contenant toutes les syllabes
    """

    phoneme = []
    phrase = cleanPhrase(phrase)
    
    
    mots = phrase.split()
    
    for mot in mots : 
        phoneme += phonetise(mot).split(" ")

    return phoneme


def verifMotIsMot(phrase, mot):
    """verifie si une image représente un mot exact 
    
    :param string phrase: phrase à rébutiser 
    :param string mot: mot à comparer
    :returns: True si une image est un mot exact, False sinon
    """

    mots = phrase.split()
   
    for m in mots : 
        if m == mot :
            return True
    return False


def bestOfTwo(sol1, sol2, phrase):
    """retourne la meilleure solution selon plusieurs critères : 
        - si les solutions contiennent des mots présent dans la phrase
        - le nombre de Mot-Image
        - la taille du Mot-Image
        - le nombre de Mot-Image d'un phonème

    :params tableau sol1: tableau de mot
    :params tableau sol2: tableau de mot
    :params string phrase: phrase de départ à comparer
    """

    # si l'une des deux solutions vaut None, on renvoit l'autre

    if sol1==[] or sol1 == None:
        return sol2
    if sol2==[] or sol2 == None:
        return sol1

    l1=len(sol1)
    l2=len(sol2)
    
    tailles1= [getLenPhonemeByMot(sol1[i]) for i in range(0, l1)]
    tailles2= [getLenPhonemeByMot(sol2[i]) for i in range(0,l2)]
   
    max1=0
    cp1=0
    for i in range(0, l1):
        if tailles1[i]==1:                  # on compte les images de taille 1 de sol1
            cp1+=1

        if tailles1[i]>max1:                # on cherche l'image la plus longue de sol1
            max1=tailles1[i]

    max2=0
    cp2=0
    for i in range(0, l2):
        if tailles2[i]==1:                  # on compte les images de taille 1 de sol2
            cp2+=1

        if tailles2[i]>max2:                # on cherche l'image la plus longue de sol1
            max2=tailles2[i]

    mot1 = 0 
    for m in sol1 :                         # on compte les mots présent dans la phrase
        if verifMotIsMot(phrase, m) : 
            mot1 += 1
    mot2 = 0
    for m in sol2 :                         # on compte les mots présent dans la phrase
        if verifMotIsMot(phrase, m) : 
            mot2 += 1


    score1 = 0
    score2 = 0

    if mot1 < mot2 :                        # sol2 est la solution contenant le moins de mots présent dans la phrase
        score1 += 3
    elif mot2 < mot1 : 
        
        score2 += 3


    if l1>l2:                               # sol2 est la sol la plus courte
        
        score2+=1
    elif l2>l1 :
        score1 +=1

    if mot1 != 0 or mot2!=0 :               # si ausun des deux solutions possèdent de mots dans la phrase
        if cp1>cp2:                         # sol2 a moins d'images de taille 1 que sol1
            score2+=3
        elif cp2 > cp1 :
            score1 +=3


    else : 
        if cp1>cp2:                         # sol2 a moins d'images de taille 1 que sol1
            
            score2+=1
        elif cp2 > cp1 :
            score1 +=1

    if max1<max2:                           # sol2 a une image max plus longue que celle de sol1
        score2+=1
    elif max1 > max2 :
        score1+=1

    if score1<=score2:
        return sol2
    else:
        return sol1


def lettreIn(sol) : 
    """si la solution contient une image représentant une lettre

    :params string sol: tableau de mot représentant une solution
    :returns: True si la solution contient une lettre, False sinon
    """
    for e in sol : 
        if len(e) == 1 : 
            return True
    return False


def rech(phrase, nbSyllabes, imagesDispo, assemblage):
    """cherche une solution possible avec des syllabes
    
    :param string phrase: phrase à rébutiser 
    :param list nbSyllabes: initialisé avec des 0, permet de connaître quelles sont les positions déjà prises. 
    :param list imagesDispo: toutes les possibilité d'image de contient le rebus
    :param list assemblage: liste contenant une solution possible
    :returns: Une solution si elle existe, None sinon
    """

    for image in imagesDispo:
        
    
        for numS in range(image[0],image[0]+len(image[len(image)-1])):
         
            

            if nbSyllabes[numS] != 0: 
                break
        else:
            newNbSyllabes=copy.copy(nbSyllabes)
            
            for numS in range(image[0],image[0]+len(image[len(image)-1])):
                
                newNbSyllabes[numS]=1
            
                
            newImagesDispo=copy.copy(imagesDispo)
            newImagesDispo.remove(image)
            newAssemblage=copy.copy(assemblage)
            newAssemblage.append(image)
    
            if not (0 in newNbSyllabes):
        
                
                motImage = []
                newAssemblage.sort()
                motsPossible = []
            

                for e in newAssemblage :
                    
                    s = " ".join(e[1]) 
                    motsPossible = getMotBySon(s)
                    if len(motsPossible) > 1 :                      # Si on a deux mot différent pour une même syllabe 
                    
                        for i in range(0, len(motsPossible)) : 
                            
                            if not verifMotIsMot(phrase, motsPossible[i][0]) : 
                            
                                motImage.append(motsPossible[i][0])
                                break
                    else :                                          # Sinon on prend le mot si il y en a qu'un seul
                        motImage.append(motsPossible[0][0])

                
                return motImage

                
            res=rech(phrase, newNbSyllabes,newImagesDispo,newAssemblage)
            
            if res != None :
                return res
            else : return None
        
       
def appelRech(app, phrase, approx):
    """Lance l'algorithem rech avec des paramètres spéficiques pour le mode syllabique et phonétique
    Cette fonction permet de lancer l'algorithme avec des syllabes, s'il y a une solution satisfaisante, elle est retournée
    mais s'il n'y a pas de solution ou que la solution est le mot en lui même alors il lance la version par phonème.

    :param string phrase: phrase à rébutiser
    :param dict approx: liste d'approximation 
    :returns: Une solution si elle existe, None sinon
    """

    with app.app_context():

        temps_max  = 120
        if approx == "" :
            approx = APPROX.copy()

       
        phrase = cleanPhrase(phrase)
        syllabes = phrase_son(phrase)
        nbS = len(syllabes)
        sol = None
        i = MAXSYL
        imagesDispo = []

        while  i >= 1 :

            nbSyllabes = [0] * nbS
            imagesDispo = allMot(syllabes, i)
           

            if imagesDispo == [] :                                  # teste s'il reste encore une solution non utilisée
                i -= 1
                sol = None

            else :
                res = rech(phrase, nbSyllabes, imagesDispo, [])
                sol = bestOfTwo(sol, res, phrase)                   # choisi la meilleure solution selon des critères précis
                if res != None : 
                        for m in res:
                            if verifMotIsMot(phrase, m):
                                for element in imagesDispo: 
                                    if element[1] == phrase_son(m):
                                        imagesDispo.remove(element)
                                        res = rech(phrase, nbSyllabes, imagesDispo, [])
                                        sol = bestOfTwo(sol, res, phrase)  
    
                if sol != None:                                     # calcule si la prochaine solution vaut le coup d'être executée ou non (si elle est possiblement meilleure que la soluion actuelle)

                    if math.ceil(nbS/i) >= len(sol) and not verifMotIsMot(phrase, sol[0]) :
                        break
                i -= 1

        motIsMot = False                                            # vérifie si la solution contient un mot dans la phrase
        if sol != None :
            for m in sol :
                if verifMotIsMot(phrase, m) :
                    motIsMot = True
                    break

        if sol == None or motIsMot == True or (sol != None and lettreIn(sol)) : # s'il n'y a pas de solution en syllabique ou un mot est présent dans la phrase ou la solution contient un mot représentant une lettre

            sol2 = None

            phonemes = phrase_son(phrase)
            phrasePho = ' '.join(phonemes)
            phrasePho = phrasePho.replace(" ", "")
            nb = MAXPHO
            nbP = len(phrasePho)
            nbPhonemes = [0] * nbP
            start_time = time.time()

            while nb >= 1 and time.time() - start_time < temps_max :    # dertermine si le temps est trop long (on note que si le temps est long, il n'y a pas de solution)

                imagesDispo = allMotPhonemes(phrasePho, nb)
                nb = len(imagesDispo[0][len(imagesDispo[0])-1])
            
                if imagesDispo == [] :      # teste s'il reste encore une solution non utilisée 

                    sol2 = None
                    break

                else:

                    res2 = rech(phrase, nbPhonemes, imagesDispo, [])
                    sol2 = bestOfTwo(sol2, res2, phrase)  # choisi la meilleure solution selon des critères précis
                    if res2 != None : 
                        for m in res2:
                            if verifMotIsMot(phrase, m):
                                for element in imagesDispo: 
                                    if element[1] == phrase_son(m):
                                        imagesDispo.remove(element)
                    
                                        res2 = rech(phrase, nbPhonemes, imagesDispo, [])
                                        sol2 = bestOfTwo(sol2, res2, phrase)  # choisi la meilleure solution selon des critères précis
                    
                     
                    if sol2 != None: # calcule si la prochaine solution vaut le coup d'être calculé ou non (si elle est possiblement meilleure que la soluion actuelle)

                        if math.ceil(nbP/nb) >= len(sol2) and not verifMotIsMot(phrase, sol2[0]):
                            break


                    nb -= 1


            if sol2 == None : #passage approximation phonétique

                for i in approx :
                    phrase2 = phrase.replace(i, approx[i])

                    if phrase != phrase2 :

                        approx.pop(i)
                        return appelRech(app, phrase2, approx)
            if sol2 != None :
                # return sol2
                return getImageFromMot(sol2)

        return getImageFromMot(sol)
        # return sol

    
def rebusDuJour(app, chemin, cpt_rebus_du_jour): 
    """Génére le rébus du jour

    :param string chemin: chemin vers le fichiers json contenant les rébus du jour
    :param int cpt_rebus_du_jour: compteur pour connaitre l'index du rébus à générer 
    :returns: un tableau de lien des images
    """

    with app.app_context():
        f = open(chemin, encoding="utf-8")                                                  
        data = json.load(f)  
     
        return appelRech(app, data['rebus'][cpt_rebus_du_jour], ""), data['rebus'][cpt_rebus_du_jour],  data['rebus'][cpt_rebus_du_jour-1]


def getNbRebusDuJour(chemin): 
    """Retourne le nombre total de rébus du jour

    :param string chemin: chemin vers le fichiers json contenant les rébus du jour
    :returns: le nombre rébus du jour 
    """

    f = open(chemin, encoding="utf-8")                                                  
    data = json.load(f)  
    return len(data['rebus']) 
